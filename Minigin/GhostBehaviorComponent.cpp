#include "MiniginPCH.h"
#include "GhostBehaviorComponent.h"
#include "GridManager.h"
#include "GameObject.h"
#include "EntityComponent.h"
#include "TextureComponent.h"

GhostBehaviorComponent::GhostBehaviorComponent(bool isPlayer) :
	m_ControlledByPlayer{isPlayer}{}

void GhostBehaviorComponent::Update(float dt)
{
	auto entity = m_wpGameObject.lock()->GetComponent<EntityComponent>();
	if (entity == nullptr) return;
	ChangeStates(dt);
	ChangeTexture();
	if (!m_ControlledByPlayer)
	{
		if (entity->CanMove())
		{
			auto& dir = entity->GetDirection();

			GetAvailableDirections(dir);
			SetRandomDirection(dir);
		}
	}
}

void GhostBehaviorComponent::SetState(GhostState s)
{
	switch (s)
	{
	case GhostState::Wander:
		m_State = s;
		break;
	case GhostState::Scared:
		if (m_State == GhostState::Wander)
		{
			m_Timer = 0.0f;
			m_State = s;
		}
		break;
	case GhostState::Dead:
		if (m_State == GhostState::Scared)
		{
			m_Timer = 0.0f;
			m_State = s;
		}
		break;
	default:
		break;
	}
}

void GhostBehaviorComponent::GetAvailableDirections(Direction currentDir)
{
	std::vector<Direction> possiblities;
	for (int i = 1; i < 5; i++)
	{
		TileType type = GridManager::GetInstance().GetNextTile(Direction(i), m_wpGameObject.lock());
		if (type != TileType::Wall)
		{
			possiblities.push_back(Direction(i));
		}
		else
		{
			m_DirectionWeights[Direction(i)] = 0.0f;
		}
	}

	float totalWeight = 0.99f;
	int size = (int)possiblities.size() - 1;
	for (auto p : possiblities)
	{
		if (p != GetOppositeDir(currentDir))
		{
			m_DirectionWeights[p] = totalWeight / size;
		}
	}
	if (currentDir != Direction::None)
	{
		m_DirectionWeights[GetOppositeDir(currentDir)] = 0.01f;
	}
}

void GhostBehaviorComponent::SetRandomDirection(Direction& currentDir)
{
	UNREFERENCED_PARAMETER(currentDir);
	std::pair<Direction, float> winningPair{};
	int maxRange{ 100 };
	int random{ 0 };
	for (auto& pair : m_DirectionWeights)
	{
		random = rand() % maxRange;
		pair.second *= random;
		if (pair.second > winningPair.second)
		{
			winningPair = pair;
		}
	}
	currentDir = winningPair.first;
}

Direction GhostBehaviorComponent::GetOppositeDir(Direction dir)
{
	switch (dir)
	{
	case Direction::None:
		break;
	case Direction::Left:
		return Direction::Right;
	case Direction::Up:
		return Direction::Down;
	case Direction::Right:
		return Direction::Left;
	case Direction::Down:
		return Direction::Up;
	default:
		break;
	}

	return Direction::None;
}

void GhostBehaviorComponent::ChangeTexture()
{
	auto textures = m_wpGameObject.lock()->GetComponents<TextureComponent>();
	switch (m_State)
	{
	case GhostState::Wander:
		textures.at(0)->Hide(true);
		textures.at(1)->Hide(true);
		textures.at(2)->Hide(false);
		break;
	case GhostState::Scared:
		textures.at(0)->Hide(false);
		textures.at(1)->Hide(true);
		textures.at(2)->Hide(true);
		break;
	case GhostState::Dead:
		textures.at(0)->Hide(true);
		textures.at(1)->Hide(false);
		textures.at(2)->Hide(true);
		break;
	default:
		break;
	}
}

void GhostBehaviorComponent::ChangeStates(float dt)
{
	if (m_State == GhostState::Scared)
	{
		m_Timer += dt;
		if (m_Timer >= 7.0f)
		{
			SetState(GhostState::Wander);
			m_Timer = 0.0f;
		}
	}
	else if (m_State == GhostState::Dead)
	{
		m_Timer += dt;
		if (m_Timer >= 5.0f)
		{
			SetState(GhostState::Wander);
			m_Timer = 0.0f;
		}
	}
}
