#pragma once
namespace MAP
{
	const int HEIGHT = 36;
	const int WIDTH = 28;
	int MAPVAR[HEIGHT][WIDTH] =
	{
	{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	{ 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,62,72, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7},
	{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,21,41, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4},
	{ 2, 1,61,31,31,71, 1,61,31,31,31,71, 1,21,41, 1,61,31,31,31,71, 1,61,31,31,71, 1, 4},
	{ 2,11,21,-1,-1,41, 1,21,-1,-1,-1,41, 1,21,41, 1,21,-1,-1,-1,41, 1,21,-1,-1,41,11, 4},
	{ 2, 1,91,51,51,81, 1,91,51,51,51,81, 1,91,81, 1,91,51,51,51,81, 1,91,51,51,81, 1, 4},
	{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4},
	{ 2, 1,61,31,31,71, 1,61,71, 1,61,31,31,31,31,31,31,71, 1,61,71, 1,61,31,31,71, 1, 4},
	{ 2, 1,91,51,51,81, 1,21,41, 1,91,51,51,71,61,51,51,81, 1,21,41, 1,91,51,51,81, 1, 4},
	{ 2, 1, 1, 1, 1, 1, 1,21,41, 1, 1, 1, 1,21,41, 1, 1, 1, 1,21,41, 1, 1, 1, 1, 1, 1, 4},
	{ 9, 5, 5, 5, 5,71, 1,21,91,31,31,71,-1,21,41,-1,61,31,31,81,41, 1,61, 5, 5, 5, 5, 8},
	{-1,-1,-1,-1,-1, 2, 1,21,61,51,51,81,-1,91,81,-1,91,51,51,71,41, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,21,41,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,21,41, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,21,41,-1,99,99,99,-1,-1,99,99,99,-1,21,41, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,91,81,-1,99,-1,-1,-1,-1,-1,-1,99,-1,91,81, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,-1,-1,-1,99,-1,-1,-1,-1,-1,-1,99,-1,-1,-1, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,61,71,-1,99,-1,-1,-1,-1,-1,-1,99,-1,61,71, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,21,41,-1,99,99,99,99,99,99,99,99,-1,21,41, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,21,41,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,21,41, 1, 4,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1, 2, 1,21,41,-1,61,31,31,31,31,31,31,71,-1,21,41, 1, 4,-1,-1,-1,-1,-1 },
	{ 6, 3, 3, 3, 3,81, 1,91,81,-1,91,51,51,71,61,51,51,81,-1,91,81, 1,91, 3, 3, 3, 3, 7},
	{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,21,41, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4 },
	{ 2, 1,61,31,31,71, 1,61,31,31,31,71, 1,21,41, 1,61,31,31,31,71, 1,61,31,31,71, 1, 4 },
	{ 2, 1,91,51,71,41, 1,91,51,51,51,81, 1,91,81, 1,91,51,51,51,81, 1,21,61,51,81, 1, 4},
	{ 2,11, 1, 1,21,41, 1, 1, 1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 1, 1, 1, 1,21,41, 1, 1,11, 4},
	{ 92,31,71, 1,21,41, 1,61,71, 1,61,31,31,31,31,31,31,71, 1,61,71, 1,21,41, 1,61,31,82 },
	{ 73,51,81, 1,91,81, 1,21,41, 1,91,51,51,71,61,51,51,81, 1,21,41, 1,91,81, 1,91,51,63 },
	{ 2, 1, 1, 1, 1, 1, 1,21,41, 1, 1, 1, 1,21,41, 1, 1, 1, 1,21,41, 1, 1, 1, 1, 1, 1, 4 },
	{ 2, 1,61,31,31,31,31,81,91,31,31,71, 1,21,41, 1,61,31,31,81,91,31,31,31,31,71, 1, 4 },
	{ 2, 1,91,51,51,51,51,51,51,51,51,81, 1,91,81, 1,91,51,51,51,51,51,51,51,51,81, 1, 4},
	{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4},
	{ 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 8},
	{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
	};
}

/*if (tileType == 99)
{
go->AddComponent<TextureComponent>("Wall.png", pos);
}
else if (tileType == 4)
{
go->AddComponent<TextureComponent>("wall_r.png", pos, scale);
}
else if (tileType == 3)
{
go->AddComponent<TextureComponent>("wall_t.png", pos, scale);
}
else if (tileType == 2)
{
go->AddComponent<TextureComponent>("wall_l.png", pos, scale);
}
else if (tileType == 5)
{
go->AddComponent<TextureComponent>("wall_b.png", pos, scale);
}
else if (tileType == 41)
{
go->AddComponent<TextureComponent>("wall_r_s.png", pos, scale);
}
else if (tileType == 31)
{
go->AddComponent<TextureComponent>("wall_t_s.png", pos, scale);
}
else if (tileType == 21)
{
go->AddComponent<TextureComponent>("wall_l_s.png", pos, scale);
}
else if (tileType == 51)
{
go->AddComponent<TextureComponent>("wall_b_s.png", pos, scale);
}
else if (tileType == 6)
{
go->AddComponent<TextureComponent>("wall_c_tl.png", pos, scale);
}
else if (tileType == 7)
{
go->AddComponent<TextureComponent>("wall_c_tr.png", pos, scale);
}
else if (tileType == 8)
{
go->AddComponent<TextureComponent>("wall_c_br.png", pos, scale);
}
else if (tileType == 9)
{
go->AddComponent<TextureComponent>("wall_c_bl.png", pos, scale);
}
else if (tileType == 61)
{
go->AddComponent<TextureComponent>("wall_cs_tl.png", pos, scale);
}
else if (tileType == 72)
{
go->AddComponent<TextureComponent>("wall_cs_tl_t.png", pos, scale);
}
else if (tileType == 73)
{
go->AddComponent<TextureComponent>("wall_cs_tl_l.png", pos, scale);
}
else if (tileType == 71)
{
go->AddComponent<TextureComponent>("wall_cs_tr.png", pos, scale);
}
else if (tileType == 62)
{
go->AddComponent<TextureComponent>("wall_cs_tr_t.png", pos, scale);
}
else if (tileType == 63)
{
go->AddComponent<TextureComponent>("wall_cs_tr_r.png", pos, scale);
}
else if (tileType == 81)
{
go->AddComponent<TextureComponent>("wall_cs_br.png", pos, scale);
}
else if (tileType == 82)
{
go->AddComponent<TextureComponent>("wall_cs_br_r.png", pos, scale);
}
else if (tileType == 91)
{
go->AddComponent<TextureComponent>("wall_cs_bl.png", pos, scale);
}
else if (tileType == 92)
{
go->AddComponent<TextureComponent>("wall_cs_bl_l.png", pos, scale);
}*/