#pragma once
#include "Singleton.h"
#include "ThreadRaii.h"

class ThreadComponent;

class ThreadManager final : public Singleton<ThreadManager>
{
public:
	void Destroy();
	void Update(float dt);
	void AddThread(std::shared_ptr<ThreadRaii> thread);
	void AddThreadComp(std::shared_ptr<ThreadComponent> threadcomp);
	void LateUpdate();

	float m_Dt = 0.0f;
private:
	std::vector<std::shared_ptr<ThreadRaii>> m_spThreads;
	std::vector<std::shared_ptr<ThreadComponent>> m_spThreadComps;
};