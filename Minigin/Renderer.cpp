#include "MiniginPCH.h"
#include "Renderer.h"
#include "TextureComponent.h"
#include "SceneManager.h"
#include <SDL.h>


void Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_Renderer == nullptr) {
		std::stringstream ss; ss << "SDL_CreateRenderer Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
	SDL_GetWindowSize(window, &m_WindowWidth, &m_WindowHeight);
}

void Renderer::Render()
{
	SDL_RenderClear(m_Renderer);
	SceneManager::GetInstance().Render();
	SDL_RenderPresent(m_Renderer);
}

void Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void Renderer::RenderTexture(SDL_Texture* texture, const float x, const float y, const float s, const float r) const
{
	SDL_Rect dst;
	SDL_QueryTexture(texture, nullptr, nullptr, &dst.w, &dst.h);
	dst.w = int(s * dst.w);
	dst.h = int(s * dst.h);

	dst.x = static_cast<int>(x - (float)dst.w / 2.0f);
	dst.y = static_cast<int>(y - (float)dst.h / 2.0f);

	SDL_RenderCopyEx(GetSDLRenderer(), texture, nullptr, &dst, r, NULL, SDL_FLIP_NONE);
}

void Renderer::RenderTexture(SDL_Texture* texture, const float2& xy, const float s, const float r) const
{
	RenderTexture(texture, xy.x, xy.y, s, r);
}

float2 Renderer::GetWindowDimensions() const
{
	return float2((float)m_WindowWidth, (float)m_WindowHeight);
}
