#include "MiniginPCH.h"
#include "Command.h"
#include "GameObject.h"
#include "Components.h"
#include "GridManager.h"
#include "EntityComponent.h"
#include "ArrowBehaviorComponent.h"

DirectionCommand::DirectionCommand(Direction dir) : m_Dir{ dir } {}

void DirectionCommand::Execute(std::shared_ptr<GameObject> entity)
{
	TileType tilr = GridManager::GetInstance().GetNextTile(m_Dir, entity);
	if (tilr != TileType::Wall)
	{
		entity->GetComponent<EntityComponent>()->GetDirection() = m_Dir;
		entity->GetComponent<TextureComponent>()->GetRotation() = ((int)m_Dir * 90.0f) + 90.0f;
	}
}

SelectCommand::SelectCommand(Direction dir) : m_Dir{ dir } {}

void SelectCommand::Execute(std::shared_ptr<GameObject> arrowObj)
{
	auto arrow = arrowObj->GetComponent<ArrowBehaviorComponent>();
	if (m_Dir == Direction::Left)
	{
		arrow->PreviousGameMode();
	}
	else if (m_Dir == Direction::Right)
	{
		arrow->NextGameMode();
	}
	else if (m_Dir == Direction::Up)
	{
		switch (arrow->GetGameMode())
		{
		case GameMode::SinglePlayer:
			arrow->CreateGame(1);
			break;
		case GameMode::MsPacmanCompany:
			arrow->CreateGame(2);
			break;
		case GameMode::GhostCompany:
			arrow->CreateGame(2, true);
			break;
		default:
			break;
		}
	}
}

