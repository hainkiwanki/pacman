#pragma once
#include "Component.h"

class GameObject;

class PlayerComponent final : public Component
{
public:
	PlayerComponent(int playerNumber);
	virtual ~PlayerComponent() = default;

	PlayerComponent(const PlayerComponent& other) = delete;
	PlayerComponent& operator=(const PlayerComponent& other) = delete;
	PlayerComponent(const PlayerComponent&& other) = delete;
	PlayerComponent& operator=(const PlayerComponent&& other) = delete;

	void Update(float dt) override;
	int GetLives() { return m_Lives; };
	void DecreaseLive();
	bool IsAlive() const;
	void CollidesWithGhost(std::shared_ptr<GameObject> ghost);

private:
	int m_Player;
	int m_Lives;
	bool m_IsAlive;
};

