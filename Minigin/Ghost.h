#pragma once
#include "GameObject.h"

enum class Color
{
	Red,
	Cyan,
	Orange,
	Pink
};

class Ghost final : public GameObject
{
	friend class GameObject;
public:
	static std::shared_ptr<Ghost> Create(Color c, float2 pos, bool isPlayer = false);
	~Ghost() = default;

	Ghost(const Ghost& obj) = delete;
	Ghost& operator=(const Ghost& obj) = delete;
	Ghost(const Ghost&& obj) = delete;
	Ghost& operator=(const Ghost&& obj) = delete;

private:
	Ghost() = default;
};

