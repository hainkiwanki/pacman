#pragma once

class Component;

class GameObject : public std::enable_shared_from_this<GameObject>
{
public:
	static std::shared_ptr<GameObject> Create(const float2& pos = float2(0.0f, 0.0f));
	~GameObject();

	GameObject(const GameObject& other) = delete;
	GameObject& operator=(const GameObject& other) = delete;
	GameObject(const GameObject&& other) = delete;
	GameObject& operator=(const GameObject&& other) = delete;

	void HandleInput();
	void Update(float dt);
	void Render();
	void SetTag(const std::string& tag);
	void SetName(const std::string& name);
	int GetAmtComponents() const { return (int)m_spComponents.size(); };
	std::string GetName() const { return m_Name; };
	std::string GetTag() const { return m_Tag; };

	std::vector<std::shared_ptr<Component>> GetAllComponents() { return m_spComponents; };

	//Templated functions
	template<class ComponentType, typename... Args>
	void AddComponent(Args&&... parmas);

	template<class ComponentType>
	std::shared_ptr<ComponentType> GetComponent();

	template<class ComponentType>
	std::vector<std::shared_ptr<ComponentType>> GetComponents();

	template<class ComponentType>
	void RemoveComponent();

protected:
	GameObject() = default;

	bool m_IsThreaded = false;
	std::string m_Name;
	std::string m_Tag;

	template<class T>
	static std::shared_ptr<T> Create();


private:
	std::vector<std::shared_ptr<Component>> m_spComponents;
	void SetParent();

	template<class CheckComponent, class ComponentType>
	bool CheckComponentAdded();

	template<class ComponentType>
	void AdditionalInit();
};


/*std::forward = It preserves the value category of its argument. Perfect 
forwarding is there to ensure that the argument provided to a function is 
forwarded to another function with the same value category 
(basically r - value vs l - value) as originally provided*/
template<class ComponentType, typename ...Args>
inline void GameObject::AddComponent(Args && ...params)
{
	if (!CheckComponentAdded<TransformComponent, ComponentType>() &&
		!CheckComponentAdded<RenderComponent, ComponentType>())
	{
		m_spComponents.push_back(std::move(std::make_shared<ComponentType>(std::forward< Args >(params)...)));
		SetParent();
		AdditionalInit<ComponentType>();
	}
	return;
}

template<class ComponentType>
inline std::shared_ptr<ComponentType> GameObject::GetComponent()
{
	const type_info& ti = typeid(ComponentType);
	for (auto component : m_spComponents)
	{
		if (component && typeid(*component) == ti)
		{
			return std::static_pointer_cast<ComponentType>(component);
		}
	}
	return nullptr;
}

template<class ComponentType>
inline std::vector<std::shared_ptr<ComponentType>> GameObject::GetComponents()
{
	std::vector<std::shared_ptr<ComponentType>> comps;
	const type_info& ti = typeid(ComponentType);
	for (auto component : m_spComponents)
	{
		if (component && typeid(*component) == ti)
		{
			comps.push_back(std::static_pointer_cast<ComponentType>(component));
		}
	}
	return comps;
}

template<class ComponentType>
inline void GameObject::RemoveComponent()
{
	if (m_spComponents.empty()) return;

	auto it = std::find_if(m_spComponents.begin(), m_spComponents.end(),
	[=](std::shared_ptr<Component> component)
	{	
		const type_info& ti = typeid(ComponentType);
		if (component && typeid(*component) == ti)
		{
			return true;
		}
		return false;
	});

	if (it != m_spComponents.end())
	{
		m_spComponents.erase(it);
	}

}

template<class T>
inline std::shared_ptr<T> GameObject::Create()
{
	std::shared_ptr<T> go(new T());
	return go;
}

template<class CheckComponent, class ComponentType>
inline bool GameObject::CheckComponentAdded()
{
	if (std::is_same<ComponentType, CheckComponent>::value)
	{
		std::shared_ptr<CheckComponent> comp = GetComponent<CheckComponent>();
		if (comp != nullptr)
		{
			return true;
		}
	}

	return false;
}

template<class ComponentType>
inline void GameObject::AdditionalInit()
{
	if (std::is_same<ComponentType, TextureComponent>::value)
	{
		AddComponent<RenderComponent>();
	}
	else if (std::is_same<ComponentType, TextComponent>::value)
	{
		AddComponent<RenderComponent>();
	}
	else if (std::is_same<ComponentType, ThreadComponent>::value)
	{
		std::static_pointer_cast<ThreadComponent>(m_spComponents[m_spComponents.size() - 1])->CreateThread();
		m_IsThreaded = true;
	}
}