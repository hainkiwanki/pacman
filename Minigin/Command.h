#pragma once
#include "Enums.h"
class GameObject;

class Command
{
public:
	virtual ~Command() = default;
	virtual void Execute(std::shared_ptr<GameObject> player) = 0;
};

class DirectionCommand final : public Command
{
public:
	DirectionCommand(Direction dir);
	virtual ~DirectionCommand() = default;
	void Execute(std::shared_ptr<GameObject> player) override;
private:
	Direction m_Dir;
};

class SelectCommand final : public Command
{
public:
	SelectCommand(Direction dir);
	~SelectCommand() = default;
	void Execute(std::shared_ptr<GameObject> arrowObj) override;
private:
	Direction m_Dir;
};