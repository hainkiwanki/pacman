#pragma once
enum class Direction : int
{
	None = 0,
	Left = 1,
	Up = 2,
	Right = 3,
	Down = 4
};

enum class ButtonInput : int
{
	C_ButtonA = 0,
	C_ButtonB = 1,
	C_ButtonX = 2,
	C_ButtonY = 3,
	C_Left = 4,
	C_Right = 5,
	C_Up = 6,
	C_Down = 7,
	K_W = 8,
	K_A = 9,
	K_S = 10,
	K_D = 11,
	K_Up = 12,
	K_Left = 13,
	K_Right = 14,
	K_Down = 15,
	K_Enter = 16,
	K_Esc = 17
};

enum class TileType
{
	Nothing,
	Wall,
	Pill,
	BigPill
};

enum class Event
{
	BigPill,
	CheckCollision
};

enum class GameMode
{
	SinglePlayer,
	MsPacmanCompany,
	GhostCompany
};