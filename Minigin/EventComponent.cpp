#include "MiniginPCH.h"
#include "EventComponent.h"
#include "ObserverComponent.h"

void EventComponent::Notify(Event e, std::shared_ptr<GameObject> other)
{
	for (auto obs : m_wpObservers)
	{
		obs->Update(e, other);
	}
}

void EventComponent::Attach(std::shared_ptr<ObserverComponent> observer)
{
	m_wpObservers.push_back(observer);
}

void EventComponent::Detach(std::shared_ptr<ObserverComponent> observer)
{
	auto it = std::find_if(m_wpObservers.begin(), m_wpObservers.end(), 
	[&](std::shared_ptr<ObserverComponent> ob)
	{
		if (observer == ob)
			return true;
		else
			return false;
	}
	);
	if (it != m_wpObservers.end())
	{
		m_wpObservers.erase(it);
	}
}