#pragma once
#include <XInput.h>
#include <memory>
#include "Singleton.h"
#include "Command.h"
#include "Enums.h"

class InputManager final : public Singleton<InputManager>
{
public:
	bool IsPressed(ButtonInput button, bool dontCheckKeyboard);
	void SetButton(ButtonInput button, std::shared_ptr<Command> command);
	void SetInput();

private:
	XINPUT_STATE currentState{};
	std::map<ButtonInput, std::shared_ptr<Command>> m_spPlayerCommands;
	int m_Counter;
	std::map<unsigned char, bool> m_LastState;
	bool CheckKeyboardPress(unsigned char c, bool ignoreCheck);
};
