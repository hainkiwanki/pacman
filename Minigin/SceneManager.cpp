#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"

std::shared_ptr<Scene> SceneManager::CreateScene(const std::string& sceneName)
{
	const auto scene = std::shared_ptr<Scene>(new Scene(sceneName));
	m_spScenes.push_back(scene);

	if (m_ActiveScene == nullptr)
		m_ActiveScene = scene;

	return scene;
}

void SceneManager::SetScene(const std::string & sceneName)
{
	if (IsValidScene(sceneName))
	{
		m_ActiveScene = IsValidScene(sceneName);
	}
}

std::shared_ptr<Scene> SceneManager::GetScene(const std::string & sceneName)
{
	return IsValidScene(sceneName);
}

void SceneManager::HandleInput()
{
	if (m_ActiveScene != nullptr)
	{
		m_ActiveScene->HandleInput();
	}
}

void SceneManager::Update(float dt)
{
	if (m_ActiveScene != nullptr)
	{
		m_ActiveScene->Update(dt);
	}
}

void SceneManager::Render() const
{
	if (m_ActiveScene != nullptr)
	{
		m_ActiveScene->Render();
	}
}

std::shared_ptr<Scene> SceneManager::IsValidScene(const std::string & name) const
{
	auto it = std::find_if(m_spScenes.begin(), m_spScenes.end(), [name](const std::shared_ptr<Scene>& scene)
	{
		if (scene->GetName() == name)
			return true;
		return false;
	});
	if (it != m_spScenes.end())
		return *(it);

	return nullptr;
}

std::shared_ptr<Scene> SceneManager::GetCurrentScene() const
{
	return m_ActiveScene;
}
