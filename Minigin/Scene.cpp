#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"

Scene::Scene(const std::string & name) :
	m_Name{ name }{}

void Scene::HandleInput()
{
	for (auto obj : m_spObjects)
	{
		obj->HandleInput();
	}
}

void Scene::Update(float dt)
{
	for (auto obj : m_spObjects)
	{
		obj->Update(dt);
	}
}

void Scene::Render() const
{
	for (auto obj : m_spObjects)
	{
		obj->Render();
	}
}

void Scene::Add(const std::shared_ptr<GameObject>& obj)
{
	m_spObjects.push_back(obj);
}