#include "MiniginPCH.h"
#include "SDL.h"
#include "Game.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "SceneManager.h"
#include "GridManager.h"
#include "UIManager.h"
#include "Scene.h"
#include "InputManager.h"
#include "ThreadManager.h"

Game::Game(const std::string& resourcePath) :
	m_ResourcePath{ resourcePath }
{
	Init();
}

Game::~Game()
{
	CleanUp();
}

void Game::HandleInput()
{
	SceneManager::GetInstance().HandleInput();
}

void Game::Update(float dt)
{
	//UNREFERENCED_PARAMETER(dt);
	UpdateFPS(dt);
	ThreadManager::GetInstance().Update(dt);
	SceneManager::GetInstance().Update(dt);
	GridManager::GetInstance().Update();
}

void Game::Render()
{
	Renderer::GetInstance().Render();
}

bool Game::StopGame()
{
	if (SceneManager::GetInstance().GetCurrentScene()->GetName() == "Game")
	{
		if (GridManager::GetInstance().GetRemainingPills() <= 0)
		{
			return true;
		}
		if (!GridManager::GetInstance().ArePlayersAlive())
		{
			return true;
		}
	}
	if (InputManager::GetInstance().IsPressed(ButtonInput::K_Esc, false))
	{
		return true;
	}

	return false;
}

void Game::LateUpdate()
{
	GridManager::GetInstance().LateUpdate();
	ThreadManager::GetInstance().LateUpdate();
}

void Game::Init()
{
	InitWindow();
	InitManagers();
	InitScenes();
}

void Game::InitWindow()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::stringstream ss; ss << "SDL_Init Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	m_Window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		448,
		576,
		SDL_WINDOW_OPENGL
	);
	if (m_Window == nullptr) {
		std::stringstream ss; ss << "SDL_CreateWindow Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
}

void Game::InitManagers()
{
	Renderer::GetInstance().Init(m_Window);
	ResourceManager::GetInstance().SetPath(m_ResourcePath);
	ResourceManager::GetInstance().Init();
	GridManager::GetInstance().Init();
}

void Game::InitScenes()
{
	auto& gridManager = GridManager::GetInstance();
	auto& sceneManager = SceneManager::GetInstance();
	auto& uiManager = UIManager::GetInstance();

	auto gameScene = sceneManager.CreateScene("Game");
	gridManager.CreateLevel(gameScene);
	uiManager.CreateUI(gameScene);

	auto menuScene = sceneManager.CreateScene("Menu");
	uiManager.CreateMenu(menuScene);

	sceneManager.SetScene("Menu");

}

void Game::CleanUp()
{
	ThreadManager::GetInstance().Destroy();
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	SDL_Quit();
}

void Game::UpdateFPS(float dt)
{
	m_FpsTimer += dt;
	if (m_FpsTimer >= 1.0f)
	{
		m_FpsTimer = 0.0f;
		UIManager::GetInstance().SetFps(int(1.0f / dt));

	}
}
