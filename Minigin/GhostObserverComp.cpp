#include "MiniginPCH.h"
#include "GhostObserverComp.h"
#include "GhostBehaviorComponent.h"
#include "GameObject.h"
#include "PlayerComponent.h"
#include "EntityComponent.h"
#include "Components.h"

void GhostObserverComp::Update(Event e, std::shared_ptr<GameObject> other)
{
	UNREFERENCED_PARAMETER(other);
	auto ghost = m_wpGameObject.lock()->GetComponent<GhostBehaviorComponent>();
	switch (e)
	{
	case Event::BigPill:
		ghost->SetState(GhostState::Scared);
		break;
	case Event::CheckCollision:
		break;;
	default:
		break;
	}
}
