#pragma once
#include "ObserverComponent.h"

class GhostObserverComp final : public ObserverComponent
{
public:
	GhostObserverComp() = default;
	virtual ~GhostObserverComp() = default;

	GhostObserverComp(const GhostObserverComp& obj) = delete;
	GhostObserverComp& operator=(const GhostObserverComp& obj) = delete;
	GhostObserverComp(const GhostObserverComp&& obj) = delete;
	GhostObserverComp& operator=(const GhostObserverComp&& obj) = delete;

	virtual void Update(Event e, std::shared_ptr<GameObject> other) override;
};

