#include "MiniginPCH.h"

std::ostream & operator<<(std::ostream & os, const float2 & rhs)
{
	os << "[ " << rhs.x << ", " << rhs.y << " ]";
	return os;
}
