#pragma once
#include "Component.h"

class GameObject;

class ObserverComponent : public Component
{
public:
	ObserverComponent() = default;
	virtual ~ObserverComponent() = default;

	ObserverComponent(const ObserverComponent& obj) = delete;
	ObserverComponent& operator=(const ObserverComponent& obj) = delete;
	ObserverComponent(const ObserverComponent&& obj) = delete;
	ObserverComponent& operator=(const ObserverComponent&& obj) = delete;

	virtual void Update(float dt) override;
	virtual void Update(Event e, std::shared_ptr<GameObject> other) = 0;

};

