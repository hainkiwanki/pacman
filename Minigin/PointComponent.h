#pragma once
#include "Component.h"
class PointComponent final : public Component
{
public:
	PointComponent(int points);
	virtual ~PointComponent() = default;

	PointComponent(const PointComponent& other) = delete;
	PointComponent& operator=(const PointComponent& other) = delete;
	PointComponent(const PointComponent&& other) = delete;
	PointComponent& operator=(const PointComponent&& other) = delete;

	virtual void Update(float dt) override;
	int GetPoints() { return m_Points; };

private:
	int m_Points;
};

