#include "MiniginPCH.h"
#include "ThreadManager.h"
#include "GameObject.h"
#include "ThreadComponent.h"

void ThreadManager::Destroy()
{
	for (auto t : m_spThreadComps)
	{
		t->Kill();
	}
}

void ThreadManager::Update(float dt)
{
	m_Dt = dt;
}

void ThreadManager::AddThread(std::shared_ptr<ThreadRaii> thread)
{
	m_spThreads.push_back(thread);
}

void ThreadManager::AddThreadComp(std::shared_ptr<ThreadComponent> threadcomp)
{
	m_spThreadComps.push_back(threadcomp);
}

void ThreadManager::LateUpdate()
{
	for (auto t : m_spThreadComps)
	{
		t->EnableUpdate();
	}
}
