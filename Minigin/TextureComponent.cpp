#include "MiniginPCH.h"
#include "TextureComponent.h"
#include "ResourceManager.h"
#include <SDL.h>

TextureComponent::TextureComponent(const std::string & path, const float2& offset, float scale, bool hidden) :
	m_Path{ path }, m_Offset{ offset }, m_Scale{ scale }, m_Rotation{ 0.0f },
	m_IsHidden{ hidden }
{
	Init();
}

TextureComponent::TextureComponent(const std::string & path, const float2 & offset, bool hidden) :
	TextureComponent(path, offset, 1.0f, hidden){}

TextureComponent::TextureComponent(const std::string & path, const float2 & offset) :
	TextureComponent(path, offset, 1.0f, false){}

TextureComponent::TextureComponent(const std::string & path, bool hidden) :
	TextureComponent(path, float2(0.0f, 0.0f), 1.0f, hidden){}

TextureComponent::TextureComponent(const std::string & path) :
	TextureComponent(path, float2(0.0f, 0.0f), 1.0f, false){}

TextureComponent::~TextureComponent()
{
	SDL_DestroyTexture(m_pTexture);
	m_pTexture = nullptr;
}

void TextureComponent::Init()
{
	m_pTexture = ResourceManager::GetInstance().LoadTexture(m_Path);
	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Width, &m_Height);
}

void TextureComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

std::ostream & operator<<(std::ostream & os, TextureComponent & texture)
{
	os << "This is a texture component: " << texture.m_Path << "[" << texture.m_Width << " x " << texture.m_Height << "]" << std::endl;	
	return os;
}
