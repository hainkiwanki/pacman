#pragma once
#include "Singleton.h"

class Scene;

class SceneManager final : public Singleton<SceneManager>
{
public:
	std::shared_ptr<Scene> CreateScene(const std::string& sceneName);
	std::shared_ptr<Scene> GetCurrentScene() const;

	void SetScene(const std::string& sceneName);
	std::shared_ptr<Scene> GetScene(const std::string& sceneName);
		
	void HandleInput();
	void Update(float dt);
	void Render() const;

private:
	std::vector<std::shared_ptr<Scene>> m_spScenes;
	std::shared_ptr<Scene> m_ActiveScene = nullptr;

	std::shared_ptr<Scene> IsValidScene(const std::string& name) const;
};

