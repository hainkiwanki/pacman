#pragma once
#include "Component.h"

struct SDL_Texture;
enum class SDL_RenderFlip;

class TextureComponent final : public Component
{
public:
	explicit TextureComponent(const std::string& path, const float2& offset, float scale, bool hidden = false);
	explicit TextureComponent(const std::string& path, const float2& offset, bool hidden);
	explicit TextureComponent(const std::string& path, const float2& offset);
	explicit TextureComponent(const std::string& path, bool hidden);
	explicit TextureComponent(const std::string& path);
	~TextureComponent();

	TextureComponent(const TextureComponent& obj) = delete;
	TextureComponent& operator=(const TextureComponent& obj) = delete;
	TextureComponent(const TextureComponent&& obj) = delete;
	TextureComponent& operator=(const TextureComponent&& obj) = delete;

	void Init();
	void Update(float dt) override;

	SDL_Texture* GetTexture() const { return m_pTexture; };
	int GetHeight() const { return m_Height; };
	int GetWidth() const { return m_Width; };
	float2& GetOffset() { return m_Offset; };
	float GetScale() const { return m_Scale; };
	float& GetRotation() { return m_Rotation; };
	bool IsHidden() { return m_IsHidden; };
	void Hide(bool hide) { m_IsHidden = hide; };
	std::string GetName() const { return m_Path; };

	friend std::ostream& operator<<(std::ostream& os, TextureComponent& texture);

private:
	std::string m_Path;
	SDL_Texture* m_pTexture;
	int m_Width;
	int m_Height;
	float2 m_Offset;
	float m_Scale;
	float m_Rotation;
	bool m_IsHidden;
};

