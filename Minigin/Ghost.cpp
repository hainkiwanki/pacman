#include "MiniginPCH.h"
#include "Ghost.h"
#include "Components.h"
#include "EntityComponent.h"
#include "GhostBehaviorComponent.h"
#include "GhostObserverComp.h"
#include "ThreadComponent.h"

std::shared_ptr<Ghost> Ghost::Create(Color c, float2 pos, bool isPlayer)
{
	std::shared_ptr<Ghost> ghost = GameObject::Create<Ghost>();
	ghost->AddComponent<TextureComponent>("GhostScared.png", float2(), 2.0f, true);
	ghost->AddComponent<TextureComponent>("GhostDead.png", float2(), 2.0f, true);
	ghost->AddComponent<TransformComponent>(pos);
	switch (c)
	{
	case Color::Red:
		ghost->AddComponent<TextureComponent>("GhostRed.png", float2(), 2.0f, false);
		ghost->SetName("ghostred");
		break;
	case Color::Cyan:
		ghost->AddComponent<TextureComponent>("GhostCyan.png", float2(), 2.0f, false);
		ghost->SetName("ghostcyan");
		break;
	case Color::Orange:
		ghost->AddComponent<TextureComponent>("GhostOrange.png", float2(), 2.0f, false);
		ghost->SetName("ghostorange");
		break;
	case Color::Pink:
		ghost->AddComponent<TextureComponent>("GhostPink.png", float2(), 2.0f, false);
		ghost->SetName("ghostpink");
		break;
	default:
		break;
	}
	ghost->AddComponent<EntityComponent>();
	if (isPlayer)
	{
		ghost->AddComponent<InputComponent>(ButtonInput::C_ButtonX, ButtonInput::C_ButtonY, ButtonInput::C_ButtonB, ButtonInput::C_ButtonA);
		ghost->GetComponent<InputComponent>()->IgnoreKeyState();
		ghost->AddComponent<GhostBehaviorComponent>(true);
	}
	else
	{
		ghost->AddComponent<GhostBehaviorComponent>();
		ghost->AddComponent<ThreadComponent>();
	}
	ghost->AddComponent<GhostObserverComp>();
	ghost->SetTag("ghost");
	return ghost;
}