#pragma once
#include "SceneManager.h"

class GameObject;

class Scene
{
	friend std::shared_ptr<Scene> SceneManager::CreateScene(const std::string& name);
public:
	~Scene() = default;
	Scene(const Scene& other) = delete;
	Scene(Scene&& other) = delete;
	Scene& operator=(const Scene& other) = delete;
	Scene& operator=(Scene&& other) = delete;

	void HandleInput();
	void Update(float dt);
	void Render() const;
	void Add(const std::shared_ptr<GameObject>& obj);
	const std::string& GetName() const { return m_Name; };

private:
	explicit Scene(const std::string& name);

	std::string m_Name;
	std::vector<std::shared_ptr<GameObject>> m_spObjects;
};

