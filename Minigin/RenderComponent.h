#pragma once
#include "Component.h"

struct SDL_Texture;

class RenderComponent final : public Component
{
public:
	RenderComponent() = default;
	~RenderComponent() = default;

	RenderComponent(const RenderComponent& obj) = delete;
	RenderComponent& operator=(const RenderComponent& obj) = delete;
	RenderComponent(const RenderComponent&& obj) = delete;
	RenderComponent& operator=(const RenderComponent&& obj) = delete;

	void Update(float dt) override;
	void Render() const;
	void Disable();
	void Enable();

private:
	bool m_ShouldRender = true ;

	void RenderTextures(const float2& pos, const std::shared_ptr<GameObject>& parent) const;
	void RenderText(const float2& pos, const std::shared_ptr<GameObject>& parent) const;

};

