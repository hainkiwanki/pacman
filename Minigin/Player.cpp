#include "MiniginPCH.h"
#include "Player.h"
#include "Components.h"
#include "EntityComponent.h"
#include "PlayerComponent.h"

int Player::m_AmtOfPlayers = 0;

std::shared_ptr<Player> Player::Create(const std::string& image, float2 pos)
{
	m_AmtOfPlayers++;
	if (m_AmtOfPlayers > 2)
	{
		std::cout << "Only 2 players allowed\n";
		throw(std::exception("ERROR: Cannot create more than 2 players\n"));
	}
	std::shared_ptr<Player> player = GameObject::Create<Player>();
	player->AddComponent<TransformComponent>(pos);
	player->AddComponent<TextureComponent>(image, float2(0.0f, 0.0f), 2.0f);
	player->AddComponent<EntityComponent>();
	player->AddComponent<PlayerComponent>(m_AmtOfPlayers);
	if (m_AmtOfPlayers == 1)
	{
		player->AddComponent<InputComponent>(ButtonInput::K_A, ButtonInput::K_W, ButtonInput::K_D, ButtonInput::K_S);
	}
	else
	{
		player->AddComponent<InputComponent>(ButtonInput::C_ButtonX, ButtonInput::C_ButtonY, ButtonInput::C_ButtonB, ButtonInput::C_ButtonA);
	}
	player->GetComponent<InputComponent>()->IgnoreKeyState();
	player->SetTag("player");
	return player;
}
