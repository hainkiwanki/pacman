#pragma once
#include "Component.h"

class TransformComponent final: public Component
{
public:
	explicit TransformComponent();
	explicit TransformComponent(float x, float y);
	explicit TransformComponent(float2 xy);
	~TransformComponent() = default;

	TransformComponent(const TransformComponent& obj) = delete;
	TransformComponent& operator=(const TransformComponent& obj) = delete;
	TransformComponent(const TransformComponent&& obj) = delete;
	TransformComponent& operator=(const TransformComponent&& obj) = delete;

	float2& GetPosition() { return m_Position; };
	float2& GetScale() { return m_Scale; };
	float2& GetRotation() { return m_Rotation; };
	void ResetPosition();

	void Update(float dt) override;

	friend std::ostream& operator<<(std::ostream& os, TransformComponent& transform);

private:
	float2 m_Position;
	float2 m_OriginalPos;
	float2 m_Scale;
	float2 m_Rotation;
};

