#include "MiniginPCH.h"
#include <vld.h>
#pragma comment(lib,"xinput.lib")
#include "SDL.h"
#include "Game.h"

#pragma warning( push )  
#pragma warning( disable : 4100 )  
int main(int argc, char* argv[]) {
#pragma warning( pop )

	srand((unsigned int)time(NULL));


	std::shared_ptr<Game> game(new Game("../Data/"));
	auto lastTime = std::chrono::high_resolution_clock::now();
	bool doContinue = true;
	while (doContinue)
	{
		auto currentTime = std::chrono::high_resolution_clock::now();
		float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count();
		lastTime = currentTime;

		game->HandleInput();
		game->Update(deltaTime);
		game->Render();
		game->LateUpdate();

		doContinue = !game->StopGame();
	}

	game.reset();

    return 0;
}