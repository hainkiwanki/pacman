#include "MiniginPCH.h"
#include "InputComponent.h"
#include "InputManager.h"
#include "Command.h"

InputComponent::InputComponent()
{}

InputComponent::InputComponent(ButtonInput left, ButtonInput up, ButtonInput right, ButtonInput down)
{
	m_Inputs[left] = std::make_shared<DirectionCommand>(Direction::Left);
	m_Inputs[up] = std::make_shared<DirectionCommand>(Direction::Up);
	m_Inputs[right] = std::make_shared<DirectionCommand>(Direction::Right);
	m_Inputs[down] = std::make_shared<DirectionCommand>(Direction::Down);
}

void InputComponent::ProcessInput()
{
	auto& input = InputManager::GetInstance();
	input.SetInput();
	for (auto p : m_Inputs)
	{
		if (input.IsPressed(p.first, m_KeyStateIgnored))
		{
			p.second->Execute(GetParent().lock());
		}
	}
}

void InputComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

bool InputComponent::BindButton(ButtonInput input, std::shared_ptr<Command> command)
{
	for (auto p : m_Inputs)
	{
		if (p.second == command)
		{
			return false;
		}
	}
	m_Inputs[input] = command;
	return true;
}

bool InputComponent::RemoveButton(ButtonInput button)
{
	auto it = m_Inputs.find(button);
	if (it == m_Inputs.end()) return false;
	m_Inputs.erase(it);

	return true;
}
