#include "MiniginPCH.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "Font.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

void ResourceManager::SetPath(const std::string& path)
{
	m_Path = path;
}

void ResourceManager::Init()
{
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::stringstream ss; ss << "Failed to load support for png: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) {
		std::stringstream ss; ss << "Failed to load support for png: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	if (TTF_Init() != 0) {
		std::stringstream ss; ss << "Failed to load support for fonts: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}
}

SDL_Texture* ResourceManager::LoadTexture(const std::string & file)
{
	SDL_Texture* temp = CheckIfLoaded(file);
	if (temp == nullptr)
	{
		std::string fullPath = (m_Path + file).c_str();
		//std::cout << (m_Path + file) << std::endl;
		SDL_Texture *texture = IMG_LoadTexture(Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
		if (texture == nullptr) {
			std::stringstream ss; ss << "Failed to load texture: " << SDL_GetError();
			throw std::runtime_error(ss.str().c_str());
		}
		m_ResourcesLoaded[file] = texture;
	}
	return m_ResourcesLoaded[file];
}
SDL_Texture * ResourceManager::CheckIfLoaded(const std::string & fileName)
{
	auto it = m_ResourcesLoaded.find(fileName);
	if (it == m_ResourcesLoaded.end())
	{
		return nullptr;
	}
	else
	{
		return m_ResourcesLoaded[fileName];
	}
}

std::shared_ptr<Font> ResourceManager::LoadFont(const std::string & file, unsigned int size)
{
	std::string fullPath = (m_Path + file).c_str();
	return std::make_shared<Font>(fullPath, size);
}
