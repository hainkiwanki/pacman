#pragma once
#include "Singleton.h"

struct SDL_Texture;
struct SDL_Window;
struct SDL_Renderer;

class Renderer final : public Singleton<Renderer>
{
	SDL_Renderer* m_Renderer = nullptr;

public:
	void Init(SDL_Window* window);
	void Render();
	void Destroy();

	void RenderTexture(SDL_Texture* texture, const float x, const float y, const float s = 1.0f, const float r = 0.0f) const;
	void RenderTexture(SDL_Texture* texture, const float2& xy, const float s = 1.0f, const float r = 0.0f) const;

	SDL_Renderer* GetSDLRenderer() const { return m_Renderer; }
	float2 GetWindowDimensions() const;

private:
	int m_WindowHeight;
	int m_WindowWidth;
};

