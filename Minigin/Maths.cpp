#include "MiniginPCH.h"
#include "Structs.h"

float2 Lerp(float2 from, float2 to, float t)
{
		return from + (to - from) * t;
}

bool IsInRect(float2 pos, SDL_Rect rect)
{
	if (pos.x <= rect.x + rect.w / 2.0f && pos.x >= rect.x - rect.w / 2.0f)
	{
		if (pos.y <= rect.y + rect.h / 2.0f && pos.y >= rect.y - rect.h / 2.0f)
		{
			return true;
		}
	}
	return false;
}
