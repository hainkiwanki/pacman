#pragma once
#include "Singleton.h"

class GameObject;
class Scene;
class Game;

class UIManager final : public Singleton<UIManager>
{
public:
	void CreateUI(std::shared_ptr<Scene> scene);
	void CreateMenu(std::shared_ptr<Scene> menu);
	void AddScore(int i);
	void SetFps(int i);

	void DecreaseLives(int player, int i);

private:
	std::shared_ptr<GameObject> m_spScore;
	std::shared_ptr<GameObject> m_spFPS;
	std::shared_ptr<GameObject> m_spLives;
	std::shared_ptr<GameObject> m_spLives2;
	int m_Score = 0;
};

