#pragma once
class Game : public std::enable_shared_from_this<Game>
{
public:
	Game(const std::string& resourcePath);
	~Game();

	void HandleInput();
	void Update(float dt);
	void LateUpdate();
	void Render();

	bool StopGame();

private:
	SDL_Window* m_Window;
	std::string m_ResourcePath;
	float m_FpsTimer = 0.0f;

	void Init();
	void InitWindow();
	void InitManagers();
	void InitScenes();
	void CleanUp();

	void UpdateFPS(float dt);
};

