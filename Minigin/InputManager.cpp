#include "MiniginPCH.h"
#include "InputManager.h"
#include "Log.h"
#include <SDL.h>

bool InputManager::IsPressed(ButtonInput button, bool dontCheckPreviousState)
{
	switch (button)
	{
	case ButtonInput::C_ButtonA:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	case ButtonInput::C_ButtonB:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case ButtonInput::C_ButtonX:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case ButtonInput::C_ButtonY:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	case ButtonInput::C_Left:
		return currentState.Gamepad.sThumbLX / 32767 == -1;
	case ButtonInput::C_Right:
		return currentState.Gamepad.sThumbLX / 32767 == 1;
	case ButtonInput::C_Down:
		return currentState.Gamepad.sThumbLY / 32767 == -1;
	case ButtonInput::C_Up:
		return currentState.Gamepad.sThumbLY / 32767 == 1;
	case ButtonInput::K_W:
		return CheckKeyboardPress('W', dontCheckPreviousState);
	case ButtonInput::K_A:
		return CheckKeyboardPress('A', dontCheckPreviousState);
	case ButtonInput::K_S:
		return CheckKeyboardPress('S', dontCheckPreviousState);
	case ButtonInput::K_D:
		return CheckKeyboardPress('D', dontCheckPreviousState);
	case ButtonInput::K_Left:
		return CheckKeyboardPress(VK_LEFT, dontCheckPreviousState);
	case ButtonInput::K_Up:
		return CheckKeyboardPress(VK_UP, dontCheckPreviousState);
	case ButtonInput::K_Right:
		return CheckKeyboardPress(VK_RIGHT, dontCheckPreviousState);
	case ButtonInput::K_Down:
		return CheckKeyboardPress(VK_DOWN, dontCheckPreviousState);
	case ButtonInput::K_Enter:
		return CheckKeyboardPress(VK_RETURN, dontCheckPreviousState);
	case ButtonInput::K_Esc:
		return CheckKeyboardPress(VK_ESCAPE, dontCheckPreviousState);
	default:
		return false;
	}
}

void InputManager::SetButton(ButtonInput button, std::shared_ptr<Command> command)
{
	m_spPlayerCommands[button] = std::move(command);
}

void InputManager::SetInput()
{
	ZeroMemory(&currentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &currentState);
}

bool InputManager::CheckKeyboardPress(unsigned char c, bool ignoreCheck)
{
	if (ignoreCheck)
	{
		return (GetAsyncKeyState(c) & 0x8000);
	}
	else
	{
		if (GetAsyncKeyState(c) & 0x8000)
		{
			if (!m_LastState[c])
			{
				m_LastState[c] = true;
				return true;
			}
		}
		if (GetAsyncKeyState(c) == 0)
			m_LastState[c] = false;

		return false;
	}
}

