#pragma once
#include <SDL.h>

float2 Lerp(float2 from, float2 to, float t);
bool IsInRect(float2 pos, SDL_Rect rect);