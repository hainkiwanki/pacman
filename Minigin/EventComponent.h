#pragma once
#include "Component.h"

class ObserverComponent;

class EventComponent: public Component
{
public:
	EventComponent() = default;
	virtual ~EventComponent() = default;

	EventComponent(const EventComponent& obj) = delete;
	EventComponent& operator=(const EventComponent& obj) = delete;
	EventComponent(const EventComponent&& obj) = delete;
	EventComponent& operator=(const EventComponent&& obj) = delete;

	virtual void Update(float dt) override { UNREFERENCED_PARAMETER(dt); };
	virtual void Notify(Event e, std::shared_ptr<GameObject> other);

	void Attach(std::shared_ptr<ObserverComponent> observer);
	void Detach(std::shared_ptr<ObserverComponent> observer);

private:
	std::vector<std::shared_ptr<ObserverComponent>> m_wpObservers;
};

