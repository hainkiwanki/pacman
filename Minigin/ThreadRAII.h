#pragma once
#include "MiniginPCH.h"

class ThreadRaii
{
	std::shared_ptr<std::thread> m_thread;
public:
	ThreadRaii(std::shared_ptr<std::thread> threadObj) : m_thread(threadObj)
	{

	}
	~ThreadRaii()
	{
		if (m_thread->joinable())
		{
			m_thread->detach();
		}
	}
};
