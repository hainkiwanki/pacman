#include "MiniginPCH.h"
#include "BigPillEvent.h"

void BigPillEvent::PillIsEaten()
{
	Notify(Event::BigPill, m_wpGameObject.lock());
}
