#include "MiniginPCH.h"
#include "TextComponent.h"
#include "Font.h"
#include "Renderer.h"
#include "ResourceManager.h"

TextComponent::TextComponent(const std::string& text, float2 offset, bool isStatic) :
	m_Text{text}, m_Offset{offset}, m_IsStatic{ isStatic }
{
	Init();
}

TextComponent::~TextComponent()
{
	SDL_DestroyTexture(m_pTexture);
	m_pTexture = nullptr;
}

void TextComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
	if (!m_IsStatic)
	{
		m_IsStatic = true;
		const SDL_Color color = { 255,255,255 };
		const auto surf = TTF_RenderText_Blended(m_spFont->GetFont(), m_Text.c_str(), color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_pTexture = texture;
	}
}

void TextComponent::SetText(const std::string & newText)
{
	m_Text = newText;
	m_IsStatic = false;
}

void TextComponent::SetOffset(float2 pos)
{
	m_Offset = pos;
}

void TextComponent::SetOffset(float x, float y)
{
	SetOffset(float2(x, y));
}

SDL_Texture * TextComponent::GetTexture() const
{
	return m_pTexture;
}

float2 TextComponent::GetOffset() const
{
	return m_Offset;
}

void TextComponent::Init()
{
	m_spFont = ResourceManager::GetInstance().LoadFont("PressStart2P.ttf", 12);
	const SDL_Color color = { 255,255,255 };
	const auto surf = TTF_RenderText_Blended(m_spFont->GetFont(), m_Text.c_str(), color);
	if (surf == nullptr)
	{
		throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
	}
	auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
	if (texture == nullptr)
	{
		throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
	}
	SDL_FreeSurface(surf);
	m_pTexture = texture;
}
