#pragma once
#include "EventComponent.h"

class BigPillEvent final : public EventComponent
{
public:
	BigPillEvent() = default;
	virtual ~BigPillEvent() = default;

	BigPillEvent(const BigPillEvent& obj) = delete;
	BigPillEvent& operator=(const BigPillEvent& obj) = delete;
	BigPillEvent(const BigPillEvent&& obj) = delete;
	BigPillEvent& operator=(const BigPillEvent&& obj) = delete;

	void PillIsEaten();
};

