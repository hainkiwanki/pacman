#pragma once
#include "Component.h"

struct SDL_Texture;
class Font;

class TextComponent final : public Component
{
public:
	explicit TextComponent(const std::string& text, float2 offset = float2(0.0f, 0.0f), bool isStatic = false);
	~TextComponent();

	TextComponent(const TextComponent& obj) = delete;
	TextComponent& operator=(const TextComponent& obj) = delete;
	TextComponent(const TextComponent&& obj) = delete;
	TextComponent& operator=(const TextComponent&& obj) = delete;

	void Update(float dt) override;
	void SetText(const std::string& newText);
	void SetOffset(float2 pos);
	void SetOffset(float x, float y);
	SDL_Texture* GetTexture() const;
	float2 GetOffset() const;

private:
	std::string m_Text;
	float2 m_Offset;
	bool m_IsStatic;
	SDL_Texture* m_pTexture;
	std::shared_ptr<Font> m_spFont;

	void Init();
};

