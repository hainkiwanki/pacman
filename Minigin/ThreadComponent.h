#pragma once
#include "Component.h"
#include <thread>

class ThreadComponent final : public Component, public std::enable_shared_from_this<ThreadComponent>
{
public:
	ThreadComponent() = default;
	virtual ~ThreadComponent() = default;

	ThreadComponent(const ThreadComponent& obj) = delete;
	ThreadComponent& operator=(const ThreadComponent& obj) = delete;
	ThreadComponent(const ThreadComponent&& obj) = delete;
	ThreadComponent& operator=(const ThreadComponent&& obj) = delete;

	void Update(float dt) override;
	void UpdateThread(float dt);
	void CreateThread();
	void EnableUpdate() { m_Update = true; };
	void Kill() { m_Dead = true; };

private:
	bool m_Update = true;
	bool m_Dead = false;
};

