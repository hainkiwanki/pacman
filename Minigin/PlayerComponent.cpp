#include "MiniginPCH.h"
#include "PlayerComponent.h"
#include "GameObject.h"
#include "Components.h"
#include "GhostBehaviorComponent.h"
#include "EntityComponent.h"
#include "UIManager.h"

PlayerComponent::PlayerComponent(int playerNumber) :
	m_Lives{ 3 }, m_Player{ playerNumber },
	m_IsAlive{ true }
{}

void PlayerComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

void PlayerComponent::DecreaseLive()
{
	if (m_Lives > 0)
	{
		auto& uimanager = UIManager::GetInstance();
		m_Lives--;
		uimanager.DecreaseLives(m_Player, m_Lives);
	}
	else
	{
		m_IsAlive = false;
	}

}

bool PlayerComponent::IsAlive() const
{
	return m_IsAlive;
}

void PlayerComponent::CollidesWithGhost(std::shared_ptr<GameObject> ghost)
{
	if (m_IsAlive)
	{
		auto ghostBehavior = ghost->GetComponent<GhostBehaviorComponent>();
		auto ghostTransform = ghost->GetComponent<TransformComponent>();
		auto ghostEntity = ghost->GetComponent<EntityComponent>();
		GhostState state = ghostBehavior->GetState();

		auto playerTransform = m_wpGameObject.lock()->GetComponent<TransformComponent>();
		auto playerEntity = m_wpGameObject.lock()->GetComponent<EntityComponent>();
		switch (state)
		{
		case GhostState::Wander:
			DecreaseLive();
			playerTransform->ResetPosition();
			playerEntity->GetDirection() = Direction::None;
			playerEntity->SetNextPosition(playerTransform->GetPosition());
			break;
		case GhostState::Scared:
			ghostBehavior->SetState(GhostState::Dead);
			ghostTransform->ResetPosition();
			ghostEntity->GetDirection() = Direction::None;
			ghostEntity->SetNextPosition(ghostTransform->GetPosition());
			UIManager::GetInstance().AddScore(200);
			break;
		case GhostState::Dead:
		default:
			break;
		}
	}
}
