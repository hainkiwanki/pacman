#include "MiniginPCH.h"
#include "TransformComponent.h"

TransformComponent::TransformComponent() :
	TransformComponent(0.0f, 0.0f){}

TransformComponent::TransformComponent(float x, float y) :
	TransformComponent(float2(x, y)){}

TransformComponent::TransformComponent(float2 xy) :
	m_Position(xy), m_OriginalPos{xy} {}

void TransformComponent::ResetPosition()
{
	m_Position = m_OriginalPos;
}

void TransformComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

std::ostream & operator<<(std::ostream & os, TransformComponent& transform)
{
	UNREFERENCED_PARAMETER(transform);
	os << "This is a transform component at position: " << transform.GetPosition();
	return os;
}