#pragma once
#include "Component.h"

class InputManager;
class Command;
enum class ButtonInput;

class InputComponent final : public Component
{
public:
	InputComponent();
	explicit InputComponent(ButtonInput left, ButtonInput up, ButtonInput right, ButtonInput down);
	~InputComponent() = default;

	InputComponent(const InputComponent& obj) = delete;
	InputComponent& operator=(const InputComponent& obj) = delete;
	InputComponent(const InputComponent&& obj) = delete;
	InputComponent& operator=(const InputComponent&& obj) = delete;

	void ProcessInput();
	void Update(float dt) override;
	bool BindButton(ButtonInput input, std::shared_ptr<Command> command);
	bool RemoveButton(ButtonInput button);
	void IgnoreKeyState() { m_KeyStateIgnored = true; };

private:
	bool m_KeyStateIgnored = false;
	std::map<ButtonInput, std::shared_ptr<Command>> m_Inputs;
};

