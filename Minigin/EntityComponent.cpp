#include "MiniginPCH.h"
#include "EntityComponent.h"
#include "Components.h"
#include "GridManager.h"
#include "GameObject.h"

EntityComponent::EntityComponent() :
	EntityComponent(Direction::None)
{}

EntityComponent::EntityComponent(Direction dir) :
	m_Dir{ dir }, m_MoveToNext{ true }, m_Timer{0.0f}
{
}

void EntityComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
	auto& transform = m_wpGameObject.lock()->GetComponent<TransformComponent>()->GetPosition();
	if (m_MoveToNext)
	{
		m_MoveToNext = false;
		m_CurrentPosition = transform;
		m_NextPosition = GridManager::GetInstance().GetNextPosition(m_Dir, m_wpGameObject.lock());
	}
	transform = Lerp(m_CurrentPosition, m_NextPosition, m_Timer / 0.2f);
	m_Timer += dt;
	if (m_Timer >= 0.2f)
	{
		m_Timer = 0.0f;
		m_MoveToNext = true;
	}
}
Direction& EntityComponent::GetDirection() 
{
	return m_Dir;
}

std::map<std::string, int>& EntityComponent::GetTilePos()
{
	return m_TilePos;
}

bool EntityComponent::CanMove() const
{
	return m_MoveToNext;
}
