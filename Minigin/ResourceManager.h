#pragma once
#include "Singleton.h"

struct SDL_Texture;
class Font;

class ResourceManager final : public Singleton<ResourceManager>
{
public:
	void SetPath(const std::string& path);
	void Init();

	SDL_Texture* LoadTexture(const std::string& file);
	std::shared_ptr<Font> LoadFont(const std::string& file, unsigned int size);

private:
	std::string m_Path;
	std::map<std::string, SDL_Texture*> m_ResourcesLoaded;

	SDL_Texture* CheckIfLoaded(const std::string& fileName);
};

