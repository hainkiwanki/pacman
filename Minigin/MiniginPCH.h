#pragma once

#include "targetver.h"

#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <tchar.h>
#include <memory> // smart pointers
#include <vector>
#include <algorithm>
#include <map>
#include <chrono>
#include <thread>
#include <stdlib.h> 
#include <ctime>
#include <thread>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <vld.h>

#include "Log.h" // Various logging functions

#include "Structs.h"
#include "Enums.h"
#include "Maths.h"