#include "MiniginPCH.h"
#include "ArrowBehaviorComponent.h"
#include "Components.h"
#include "GameObject.h"
#include "Game.h"
#include "Player.h"
#include "Ghost.h"
#include "GridManager.h"
#include "SceneManager.h"

ArrowBehaviorComponent::ArrowBehaviorComponent() :
	m_Mode(GameMode::SinglePlayer)
{}

ArrowBehaviorComponent::~ArrowBehaviorComponent()
{}

void ArrowBehaviorComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
	float2& pos = m_wpGameObject.lock()->GetComponent<TransformComponent>()->GetPosition();
	switch (m_Mode)
	{
	case GameMode::SinglePlayer:
		pos = float2(96.0f, 320.0f);
		break;
	case GameMode::MsPacmanCompany:
		pos = float2(208.0f, 320.0f);
		break;
	case GameMode::GhostCompany:
		pos = float2(320.0f, 320.0f);
		break;
	default:
		break;
	}
}

void ArrowBehaviorComponent::NextGameMode()
{
	switch (m_Mode)
	{
	case GameMode::SinglePlayer:
		m_Mode = GameMode::MsPacmanCompany;
		break;
	case GameMode::MsPacmanCompany:
		m_Mode = GameMode::GhostCompany;
		break;
	case GameMode::GhostCompany:
		m_Mode = GameMode::SinglePlayer;
		break;
	default:
		break;
	}
}

void ArrowBehaviorComponent::PreviousGameMode()
{
	switch (m_Mode)
	{
	case GameMode::SinglePlayer:
		m_Mode = GameMode::GhostCompany;
		break;
	case GameMode::MsPacmanCompany:
		m_Mode = GameMode::SinglePlayer;
		break;
	case GameMode::GhostCompany:
		m_Mode = GameMode::MsPacmanCompany;
		break;
	default:
		break;
	}
}

void ArrowBehaviorComponent::CreateGame(int players, bool isGhost)
{
	auto& gridManager = GridManager::GetInstance();
	auto& sceneManager = SceneManager::GetInstance();

	std::shared_ptr<Player> player1 = Player::Create("Pacman.png", float2(208.f, 424.0f));
	gridManager.AddPlayer(player1, "pacman");
	std::shared_ptr<Ghost> ghost2 = Ghost::Create(Color::Cyan, float2(192.0f, 280.0f));
	gridManager.AddGhost(ghost2);
	std::shared_ptr<Ghost> ghost3 = Ghost::Create(Color::Pink, float2(224.0f, 280.0f));
	gridManager.AddGhost(ghost3);
	std::shared_ptr<Ghost> ghost4 = Ghost::Create(Color::Orange, float2(256.0f, 280.0f));
	gridManager.AddGhost(ghost4);
	if (players == 1)
	{
		std::shared_ptr<Ghost> ghost = Ghost::Create(Color::Red, float2(224.0f, 232.0f));
		gridManager.AddGhost(ghost);
	}
	else
	{
		if (isGhost)
		{
			std::shared_ptr<Ghost> ghost = Ghost::Create(Color::Red, float2(224.0f, 232.0f), true);
			gridManager.AddGhost(ghost);
		}
		else
		{
			std::shared_ptr<Player> player2 = Player::Create("PacmanMs.png", float2(224.f, 424.0f));
			gridManager.AddPlayer(player2, "mspacman");
			std::shared_ptr<Ghost> ghost = Ghost::Create(Color::Red, float2(224.0f, 232.0f));
			gridManager.AddGhost(ghost);
		}
	}
	gridManager.Initialized();
	sceneManager.SetScene("Game");
}
