#include "MiniginPCH.h"
#include "GridManager.h"
#include "GameObject.h"
#include "Components.h"
#include "EntityComponent.h"
#include "GhostObserverComp.h"
#include "Events.h"
#include "PointComponent.h"
#include "Scene.h"
#include "Map.h"
#include "Renderer.h"
#include "Enums.h"
#include "UIManager.h"
#include "PlayerComponent.h"

void GridManager::CreateLevel(std::shared_ptr<Scene> scene)
{
	m_Scene = scene;
	std::shared_ptr<GameObject> map = GameObject::Create();
	map->AddComponent<TextureComponent>("map.png", Renderer::GetInstance().GetWindowDimensions() / 2.0f);
	m_Scene->Add(std::move(map));

	float2 startPos{ 8.0f, 8.0f };
	for (int i = 0; i < MAP::HEIGHT; i++) //36 rows
	{
		for(int j = 0; j < MAP::WIDTH; j++) //28 columns
		{
			int index = i * MAP::WIDTH + j;
			m_TilePositions[index] = startPos;
			std::shared_ptr<GameObject> gridPiece = GameObject::Create();
			SetTileType(MAP::MAPVAR[i][j], index);
			ParseGridTile(MAP::MAPVAR[i][j], gridPiece, startPos);
			if (m_TileTypes[index] == TileType::BigPill || m_TileTypes[index] == TileType::Pill)
			{
				if (m_TileTypes[index] == TileType::BigPill)
				{
					gridPiece->AddComponent<PointComponent>(50);
				}
				else
				{
					gridPiece->AddComponent<PointComponent>(10);
				}
				m_spPills[index] = gridPiece;
			}
			m_Scene->Add(std::move(gridPiece));
			startPos.x += 16.0f;
		}
		startPos.y += 16.0f;
		startPos.x = 8.0f;
	}
	m_PillCounter = (int)m_spPills.size();
}

void GridManager::AddPlayer(std::shared_ptr<GameObject> entity, const std::string& tag)
{
	m_spEntities[tag] = entity;
	EntityTilePos(m_spEntities[tag]);
	m_Scene->Add(m_spEntities[tag]);
}

void GridManager::AddGhost(std::shared_ptr<GameObject> entity)
{
	std::string s{ "ghost" + std::to_string(m_GhostCounter) };
	m_spEntities[s] = entity;
	EntityTilePos(m_spEntities[s]);
	m_GhostCounter++;
	AddToEventObservers<GhostObserverComp>(m_spEntities[s]);
	m_Scene->Add(m_spEntities[s]);
}

void GridManager::Init()
{
	m_EventEmitter = GameObject::Create();
	m_EventEmitter->AddComponent<BigPillEvent>();
}

void GridManager::Update()
{
	for (auto p : m_spEntities)
	{
		EntityTilePos(p.second);
	}
}

void GridManager::LateUpdate()
{
	for (auto p : m_spEntities)
	{
		if(p.second->GetTag() == "player")
			CheckForCollision(p.second);
	}
}

float2 GridManager::GetNextPosition(Direction dir, std::shared_ptr<GameObject> entity)
{
	float2 result = entity->GetComponent<TransformComponent>()->GetPosition();
	for (auto p : m_spEntities)
	{
		if (p.second == entity)
		{
			auto& tiles = p.second->GetComponent<EntityComponent>()->GetTilePos();
			int index = CalculateNextIndex(tiles["x"], tiles["y"], dir, true);
			if (m_TileTypes[index] != TileType::Wall)
			{
				result = m_TilePositions[index];
				break;
			}
		}
	}
	return result;
}

TileType GridManager::GetNextTile(Direction dir, std::shared_ptr<GameObject> entity)
{
	int index{0};
	for (auto p : m_spEntities)
	{
		if (p.second == entity)
		{
			auto& tiles = p.second->GetComponent<EntityComponent>()->GetTilePos();
			index = CalculateNextIndex(tiles["x"], tiles["y"], dir);
			break;
		}
	}
	return m_TileTypes[index];
}

int GridManager::GetRemainingPills()
{
	return m_PillCounter;
}

bool GridManager::ArePlayersAlive() const
{
	bool player2IsAlive = false;
	auto it = m_spEntities.find("mspacman");
	if (it != m_spEntities.end())
		player2IsAlive = it->second->GetComponent<PlayerComponent>()->IsAlive();

	bool playerIsAlive = false;
	it = m_spEntities.find("pacman");
	if (it != m_spEntities.end())
		playerIsAlive = it->second->GetComponent<PlayerComponent>()->IsAlive();

	return (playerIsAlive || player2IsAlive);
}

void GridManager::EntityTilePos(std::shared_ptr<GameObject> entity)
{
	int size = 16;
	auto pos = entity->GetComponent<TransformComponent>()->GetPosition();
	auto& tiles = entity->GetComponent<EntityComponent>()->GetTilePos();
	for (auto p : m_TilePositions)
	{
		SDL_Rect rect;
		rect.w = size;
		rect.h = size;
		rect.x = (int)p.second.x;
		rect.y = (int)p.second.y;
		if (IsInRect(pos, rect))
		{
			tiles["x"] = p.first % MAP::WIDTH;
			tiles["y"] = p.first / MAP::WIDTH;
			if (m_IsInitialized)
			{
				if (m_TileTypes[p.first] == TileType::Pill || m_TileTypes[p.first] == TileType::BigPill)
				{
					if (entity->GetTag() == "player")
					{
						if (m_TileTypes[p.first] == TileType::BigPill)
							m_EventEmitter->GetComponent<BigPillEvent>()->PillIsEaten();
						int score = m_spPills[p.first]->GetComponent<PointComponent>()->GetPoints();
						UIManager::GetInstance().AddScore(score);
						m_spPills[p.first]->GetComponent<RenderComponent>()->Disable();
						m_TileTypes[p.first] = TileType::Nothing;
						m_PillCounter--;
					}
				}
			}
		}
	}
}

void GridManager::ParseGridTile(int tileType, std::shared_ptr<GameObject> go, float2 pos)
{
	float scale = 16.0f / 24.0f;
	if (tileType > 1)
	{
		if (tileType == 11)
		{
			go->AddComponent<TextureComponent>("pill_b.png", pos, scale);
		}
	}
	else if(tileType == 1)
	{
		go->AddComponent<TextureComponent>("pill_s.png", pos, scale);
	}
}

void GridManager::SetTileType(int tileTypeIndex, int index)
{
	if (tileTypeIndex > 1)
	{
		if(tileTypeIndex == 11)
			m_TileTypes[index] = TileType::BigPill;
		else
			m_TileTypes[index] = TileType::Wall;
	}
	else if (tileTypeIndex == 1)
	{
		m_TileTypes[index] = TileType::Pill;
	}
	else
	{
		m_TileTypes[index] = TileType::Nothing;
	}
}

int GridManager::CalculateNextIndex(int& x, int& y, Direction dir, bool change)
{
	int xTile{ x }, yTile{ y };
	switch (dir)
	{
	case Direction::None:
		break;
	case Direction::Left:
		xTile--;
		break;
	case Direction::Up:
		yTile--;
		break;
	case Direction::Right:
		xTile++;
		break;
	case Direction::Down:
		yTile++;
		break;
	default:
		break;
	}
	int index = yTile * MAP::WIDTH + xTile;

	if (change)
	{
		x = xTile;
		y = yTile;
	}
	return index;
}

void GridManager::CheckForCollision(std::shared_ptr<GameObject> entity)
{
	auto player = entity;
	for (auto p : m_spEntities)
	{
		if (p.second->GetTag() == "ghost")
		{
			auto posGhost = p.second->GetComponent<EntityComponent>()->GetTilePos();
			auto posPlayer = player->GetComponent<EntityComponent>()->GetTilePos();
			if ((posGhost["x"] == posPlayer["x"]) &&
				(posGhost["y"] == posPlayer["y"]))
			{
				player->GetComponent<PlayerComponent>()->CollidesWithGhost(p.second);
			}
		}
	}
}