#pragma once

class GameObject;

class Component
{
public:
	Component() = default;
	virtual ~Component() = default;

	Component(const Component& obj) = delete;
	Component& operator=(const Component& obj) = delete;
	Component(const Component&& obj) = delete;
	Component& operator=(const Component&& obj) = delete;

	virtual void Update(float dt) = 0;
	virtual void SetParent(std::weak_ptr<GameObject> go);
	virtual std::weak_ptr<GameObject> GetParent() const { return m_wpGameObject; };

protected:
	std::weak_ptr<GameObject> m_wpGameObject;
};

