#pragma once
#include "Component.h"
#include "Enums.h"

class EntityComponent final : public Component
{
public:
	explicit EntityComponent();
	explicit EntityComponent(Direction dir);
	~EntityComponent() = default;

	EntityComponent(const EntityComponent& obj) = delete;
	EntityComponent& operator=(const EntityComponent& obj) = delete;
	EntityComponent(const EntityComponent&& obj) = delete;
	EntityComponent& operator=(const EntityComponent&& obj) = delete;

	virtual void Update(float dt) override;
	Direction& GetDirection();
	std::map<std::string, int>& GetTilePos();
	bool CanMove() const;
	void SetNextPosition(float2 pos) { m_NextPosition = pos; };

private:
	Direction m_Dir;
	std::map<std::string, int> m_TilePos;
	float2 m_NextPosition;
	float2 m_CurrentPosition;
	float m_Timer;
	float m_MoveToNext;
};

