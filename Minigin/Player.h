#pragma once
#include "GameObject.h"
class Player final : public GameObject
{
	friend class GameObject;
public:
	static std::shared_ptr<Player> Create(const std::string& image, float2 pos);
	~Player() = default;

	Player(const Player& obj) = delete;
	Player& operator=(const Player& obj) = delete;
	Player(const Player&& obj) = delete;
	Player& operator=(const Player&& obj) = delete;

private:
	Player() = default;

private:
	static int m_AmtOfPlayers;
};

