#include "MiniginPCH.h"
#include "GameObject.h"
#include "Components.h"
#include "Renderer.h"

void RenderComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

void RenderComponent::Render() const
{
	if (m_ShouldRender)
	{
		auto gameobject = GetParent();
		float2 pos(0.0f, 0.0f);

		auto transform = gameobject.lock()->GetComponent<TransformComponent>();
		if (transform)
		{
			pos += float2(transform->GetPosition().x, transform->GetPosition().y);
		}

		RenderTextures(pos, gameobject.lock());
		RenderText(pos, gameobject.lock());
	}
}

void RenderComponent::Disable()
{
	m_ShouldRender = false;
}

void RenderComponent::Enable()
{
	m_ShouldRender = true;
}

void RenderComponent::RenderTextures(const float2 & pos, const std::shared_ptr<GameObject>& parent) const
{
	auto textures = parent->GetComponents<TextureComponent>();
	if (textures.size() == 0) return;

	for (auto t : textures)
	{
		float2 actualPos{ pos };
		if (!t->IsHidden())
		{
			actualPos += t->GetOffset();
			Renderer::GetInstance().RenderTexture(t->GetTexture(), actualPos, t->GetScale(), t->GetRotation());
		}
	}
}

void RenderComponent::RenderText(const float2 & pos, const std::shared_ptr<GameObject>& parent) const
{
	auto texts = parent->GetComponents<TextComponent>();
	if (texts.size() == 0) return;
	
	float2 actualPos{ pos };
	for (auto t : texts)
	{
		actualPos += t->GetOffset();
		Renderer::GetInstance().RenderTexture(t->GetTexture(), actualPos, 1.0f);
	}
}
