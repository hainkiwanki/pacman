#include "MiniginPCH.h"
#include "UIManager.h"
#include "Scene.h"
#include "GameObject.h"
#include "Components.h"
#include "Command.h"
#include "ArrowBehaviorComponent.h"

void UIManager::CreateUI(std::shared_ptr<Scene> scene)
{
	std::shared_ptr<GameObject> uiText = GameObject::Create();
	uiText->AddComponent<TextComponent>("SCORE", float2(96.0f, 16.0f));
	scene->Add(std::move(uiText));

	m_spFPS = GameObject::Create();
	m_spFPS->AddComponent<TextComponent>("00", float2(24.0f, 24.0f));
	scene->Add(m_spFPS);

	m_spScore = GameObject::Create(float2(96.0f, 32.0f));
	m_spScore->SetName("score");
	m_spScore->AddComponent<TextComponent>(std::to_string(m_Score));
	scene->Add(m_spScore);

	m_spLives = GameObject::Create(float2(32.0f, 560.0f));
	m_spLives->AddComponent<TextureComponent>("Pacman.png");
	m_spLives->AddComponent<TextureComponent>("Pacman.png", float2(16.0f, 0.0f));
	m_spLives->AddComponent<TextureComponent>("Pacman.png", float2(32.0f, 0.0f));
	scene->Add(m_spLives);

	m_spLives2 = GameObject::Create(float2(384.0f, 560.0f));
	m_spLives2->AddComponent<TextureComponent>("PacmanMs.png");
	m_spLives2->AddComponent<TextureComponent>("PacmanMs.png", float2(16.0f, 0.0f));
	m_spLives2->AddComponent<TextureComponent>("PacmanMs.png", float2(32.0f, 0.0f));
	scene->Add(m_spLives2);
}

void UIManager::CreateMenu(std::shared_ptr<Scene> menu)
{
	std::shared_ptr<GameObject> menuObj = GameObject::Create();
	menuObj->AddComponent<TextureComponent>("Pacman.png", float2(96.0f, 288.0f), 2.0f);
	menuObj->AddComponent<TextureComponent>("player2_1.png", float2(208.0f, 288.0f), 2.0f);
	menuObj->AddComponent<TextureComponent>("player2_2.png", float2(320.0f, 288.0f), 2.0f);
	
	std::shared_ptr<GameObject> arrow = GameObject::Create(float2(96.0f, 320.0f));
	arrow->AddComponent<TextureComponent>("arrow.png");
	arrow->AddComponent<InputComponent>();
	arrow->AddComponent<ArrowBehaviorComponent>();
	auto input = arrow->GetComponent<InputComponent>();
	input->BindButton(ButtonInput::K_Left, std::make_shared<SelectCommand>(Direction::Left));
	input->BindButton(ButtonInput::K_Right, std::make_shared<SelectCommand>(Direction::Right));
	input->BindButton(ButtonInput::K_Enter, std::make_shared<SelectCommand>(Direction::Up));

	menu->Add(arrow);
	menu->Add(menuObj);
}

void UIManager::AddScore(int i)
{
	m_Score += i;
	m_spScore->GetComponent<TextComponent>()->SetText(std::to_string(m_Score));
}

void UIManager::SetFps(int i)
{
	m_spFPS->GetComponent<TextComponent>()->SetText(std::to_string(i));
}

void UIManager::DecreaseLives(int player, int i)
{
	if (player == 1)
	{
		auto lives = m_spLives->GetComponents<TextureComponent>();
		lives[i]->Hide(true);
	}
	else
	{
		auto lives = m_spLives2->GetComponents<TextureComponent>();
		lives[i]->Hide(true);
	}
}
