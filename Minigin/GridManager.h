#pragma once
#include "Singleton.h"

class Scene;
class GameObject;
enum class Direction;

class GridManager final : public Singleton<GridManager>
{
public:
	void CreateLevel(std::shared_ptr<Scene> scene);
	void AddPlayer(std::shared_ptr<GameObject> entity, const std::string& tag);
	void AddGhost(std::shared_ptr<GameObject> entity);
	void Init();
	void Update();
	void LateUpdate();
	float2 GetNextPosition(Direction dir, std::shared_ptr<GameObject> entity);
	TileType GetNextTile(Direction dir, std::shared_ptr<GameObject> entity);
	int GetRemainingPills();
	void Initialized() { m_IsInitialized = true; };
	bool ArePlayersAlive() const;

private:
	bool m_IsInitialized{ false };
	int m_GhostCounter{ 1 };
	int m_PillCounter{ 0 };
	std::map<std::string, std::shared_ptr<GameObject>> m_spEntities; //All entities moving on the map
	std::map<int, float2> m_TilePositions;					//Position of tiles
	std::map<int, TileType> m_TileTypes;					//Tile type per tile
	std::map<int, std::shared_ptr<GameObject>> m_spPills;	//Locations of pills
	std::shared_ptr<Scene> m_Scene;
	std::shared_ptr<GameObject> m_EventEmitter;

	void EntityTilePos(std::shared_ptr<GameObject> entity);
	void ParseGridTile(int tileType, std::shared_ptr<GameObject> go, float2 pos);
	void SetTileType(int tileTypeIndex, int index);
	int CalculateNextIndex(int& x, int& y, Direction dir, bool change = false);
	void CheckForCollision(std::shared_ptr<GameObject> entity);

	template <class T>
	void AddToEventObservers(std::shared_ptr<GameObject> entity);
};

template<class T>
inline void GridManager::AddToEventObservers(std::shared_ptr<GameObject> entity)
{
	auto eventPill = m_EventEmitter->GetComponent<BigPillEvent>();
	if (eventPill != nullptr)
	{
		eventPill->Attach(entity->GetComponent<T>());
	}
}
