#pragma once
struct float2
{
	float2(float X, float Y) : x{ X }, y{ Y } {};
	float2() : float2(0.0f, 0.0f) {};

	float x;
	float y;

	inline float2 operator+(float2 f2)
	{
		return float2(x + f2.x, y + f2.y);
	}
	inline float2 operator-(float2 f2)
	{
		return float2(x - f2.x, y - f2.y);
	}
	inline float2 operator*(float f)
	{
		return float2(x * f, y * f);
	}
	inline float2 operator/(float f)
	{
		return float2(x / f, y / f);
	}
	inline float2 operator=(float2 f2)
	{
		x = f2.x;
		y = f2.y;
		return *this;
	}
	inline float2& operator+=(float2 f2)
	{
		x += f2.x;
		y += f2.y;
		return *this;
	}
	friend std::ostream& operator<< (std::ostream& os, const float2& rhs);
};