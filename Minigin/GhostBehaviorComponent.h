#pragma once
#include "Component.h"

enum class GhostState
{
	Wander,
	Scared,
	Dead
};

class GhostBehaviorComponent : public Component
{
public:
	explicit GhostBehaviorComponent(bool isPlayer = false);
	~GhostBehaviorComponent() = default;

	GhostBehaviorComponent(const GhostBehaviorComponent& obj) = delete;
	GhostBehaviorComponent& operator=(const GhostBehaviorComponent& obj) = delete;
	GhostBehaviorComponent(const GhostBehaviorComponent&& obj) = delete;
	GhostBehaviorComponent& operator=(const GhostBehaviorComponent&& obj) = delete;

	void Update(float dt) override;

	void SetState(GhostState s);
	GhostState GetState() { return m_State; };

private:
	std::map<Direction, float> m_DirectionWeights;
	GhostState m_State = GhostState::Wander;
	float m_Timer = 0.0f;
	bool m_ControlledByPlayer;

	void GetAvailableDirections(Direction currentDir);
	void SetRandomDirection(Direction& currentDir);
	Direction GetOppositeDir(Direction dir);
	void ChangeTexture();
	void ChangeStates(float dt);
};

