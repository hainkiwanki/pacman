#include "MiniginPCH.h"
#include "ThreadComponent.h"
#include "ThreadManager.h"
#include "GameObject.h"
#include "ThreadComponent.h"

void ThreadComponent::Update(float dt)
{
	UNREFERENCED_PARAMETER(dt);
}

void ThreadComponent::UpdateThread(float dt)
{
	UNREFERENCED_PARAMETER(dt);
	while (!m_Dead)
	{
		while (m_Update)
		{
			for (auto p : m_wpGameObject.lock()->GetAllComponents())
			{
				p->Update(dt);
			}
			m_Update = false;
		}
	}
}

void ThreadComponent::CreateThread()
{
	auto& threadManager = ThreadManager::GetInstance();
	std::shared_ptr<std::thread> threadobj = std::make_shared<std::thread>(
		&ThreadComponent::UpdateThread, 
		this,
		threadManager.m_Dt);

	threadManager.AddThread(std::make_shared<ThreadRaii>(threadobj));
	threadManager.AddThreadComp(shared_from_this());
}

