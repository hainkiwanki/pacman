#include "MiniginPCH.h"
#include "GameObject.h"
#include "Components.h"
#include "Renderer.h"


std::shared_ptr<GameObject> GameObject::Create(const float2& pos)
{
	std::shared_ptr<GameObject> go(new GameObject());          
	go->AddComponent<TransformComponent>(pos);
	return go;
}

GameObject::~GameObject()
{}

void GameObject::HandleInput()
{
	auto inputComp = GetComponent<InputComponent>();
	if (inputComp != nullptr)
	{
		inputComp->ProcessInput();
	}
}

void GameObject::Update(float dt)
{
	if (!m_IsThreaded)
	{
		for (auto c : m_spComponents)
		{
			c->Update(dt);
		}
	}
}

void GameObject::Render()
{
	auto render = GetComponent<RenderComponent>();
	if (render == nullptr) return;
	render->Render();
}

void GameObject::SetTag(const std::string & tag)
{
	m_Tag = tag;
}

void GameObject::SetName(const std::string & name)
{
	m_Name = name;
}

void GameObject::SetParent()
{
	m_spComponents[m_spComponents.size() - 1]->SetParent(shared_from_this());
}
