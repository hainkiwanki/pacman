#pragma once
#include "Component.h"
#include "Enums.h"
#include "Game.h"

class ArrowBehaviorComponent final : public Component
{
public:
	ArrowBehaviorComponent();
	virtual ~ArrowBehaviorComponent();

	ArrowBehaviorComponent(const ArrowBehaviorComponent& obj) = delete;
	ArrowBehaviorComponent& operator=(const ArrowBehaviorComponent& obj) = delete;
	ArrowBehaviorComponent(const ArrowBehaviorComponent&& obj) = delete;
	ArrowBehaviorComponent& operator=(const ArrowBehaviorComponent&& obj) = delete;

	void Update(float dt) override;

	void SetGameMode(GameMode mode) { m_Mode = mode; };
	GameMode GetGameMode() const { return m_Mode; };
	void NextGameMode();
	void PreviousGameMode();
	void CreateGame(int players, bool isGhost = false);

private:
	GameMode m_Mode;
};

